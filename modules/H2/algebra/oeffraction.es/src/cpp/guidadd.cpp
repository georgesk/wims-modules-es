target=guidadd

#include "header.inc"
\computeanswer{no}

\title{Suma guiada}

\integer{den1=random(8,9,10,12,14,15,16)}
\integer{den2=random(randint(\den1/2+1..\den1-1),randint(\den1+1..2*\den1-1))}
\integer{num1=randint(2..1.5*\den1)}
\integer{num1=gcd(\num1,\den1)>1?\num1+1}
\integer{num2=randint(2..1.5*\den2)}
\integer{num2=gcd(\num2,\den2)>1?\num2+1}
\text{hr=<HR noshade size=2>}
\text{new=}
\integer{test1=0}
\text{big1=<font size=+2>}
\text{big2=</font>}
\text{objets=s�lo el numerador,s�lo el denominador,
el numerador y el denominador}

\text{gtext1=random(Reescribir las dos fracciones para que tengan
    el mismo denominador,
    Igualar el denominador de las dos fracciones
)}

\text{method1=\gtext1,
    Reescribir las dos fracciones para que tengan el mismo numerador,
    Sumar los numeradores primero,
    Sumar los denominadores primero,
    Sumar los numeradores y multiplicar los denominadores,
    Multiplicar los numeradores
}
\integer{cnt1=items(\method1)}
\text{sh1=shuffle(\cnt1)}
\integer{good1=position(1,\sh1)}
\text{method1=item(\sh1,\method1)}

\text{method2=Sumar los numeradores y guardar el denominador com�n,
 Sumar separadamente los numeradores y los denominadores
}
\integer{cnt2=items(\method2)}
\text{sh2=shuffle(\cnt2)}
\integer{good2=position(1,\sh2)}
\text{method2=item(\sh2,\method2)}

\text{nstep=r1}
\nextstep{\nstep}

\statement{Calcula la siguiente suma.
<p><center>
\(\num1/(\den1) + \num2/(\den2))
</center> <p>

\if{\step=1}{
 Elige el m�todo inicial: <ul>
 \for{k=1 to \cnt1}{ <li>\embed{r1,\k} }
 </ul>
 \exit{}
}

\if{\step=2 or \test1<1}{
 Etapa 1. Igualar los denominadores de las fracciones.
 <p>
 Multiplicando \embed{r2} de \(\num1/(\den1)) por \embed{r3,3}, se obtiene
 <p><center><table border=0 cellpadding=3>
  <tr>
  <td align=center>\num1<td rowspan=3>&nbsp;\big1=\big2&nbsp;
  <td align=center>\embed{r6,3}
  <tr><td>\hr<td>\hr
  <tr><td align=center>\den1
  <td align=center>\embed{r7,3}
 </table></center> <p>
 Multiplicando \embed{r4} de \(\num2/(\den2)) por \embed{r5,3}, se obtiene
 <p><center><table border=0 cellpadding=3>
  <tr>
  <td align=center>\num2<td rowspan=3>&nbsp;\big1=\big2&nbsp;
  <td align=center>\embed{r8,3}
  <tr><td>\hr<td>\hr
  <tr><td align=center>\den2
  <td align=center>\embed{r9,3}
 </table></center> <p>
 \exit{}
}
 Etapa 1. Se tiene \(\num1/(\den1) = \new[1]/(\new[2])) y
 \(\num2/(\den2) = \new[3]/(\new[4])), luego la suma es
 <p><center>
  \(\new[1]/(\new[2]) + \new[3]/(\new[4])) .
 </center> <p>
 \if{\step=3}{
  �Cu�l es la siguiente etapa?<ul>
  \for{k=1 to \cnt2}{ <li>\embed{r10,\k} }
  </ul>
  \exit{}
 }
 Etapa 2. Sumamos los numeradores y guardamos el denominador com�n, luego
 la suma es
 <p><center>
 <table border=0 cellpadding=2>
 <tr><td align=center>\num1
 <td rowspan=3>&nbsp;&nbsp;\big1+\big2&nbsp;&nbsp;</td>
 <td align=center>\num2
 <td rowspan=3>&nbsp;&nbsp;&nbsp;\big1=\big2&nbsp;&nbsp;&nbsp;</td>
 <td align=center>\new[1]
 <td rowspan=3>&nbsp;&nbsp;\big1+\big2&nbsp;&nbsp;</td>
 <td align=center>\new[3]
 <td rowspan=3>&nbsp;&nbsp;&nbsp;\big1=\big2&nbsp;&nbsp;&nbsp;</td>
 <td align=center>\embed{r11,4}
 <tr><td>\hr<td>\hr<td>\hr<td>\hr<td>\hr
 <tr><td align=center>\den1
 <td align=center>\den2
 <td align=center>\new[2]
 <td align=center>\new[4]
 <td align=center>\embed{r12,4}
 </table> 
 </center> <p>

}

\answer{M�todo etapa 1}{\good1;\method1}{type=click}

\answer{Objetos que hay que multiplicar 1}{3;\objets}{type=menu}
\answer{Multiplicador 1}{\m1}{type=number}
\answer{Objetos que hay que multiplicar 2}{3;\objets}{type=menu}
\answer{Multiplicador 2}{\m2}{type=number}
\answer{Nuevo numerador 1}{\nn1}{type=number}
\answer{Nuevo denominador 1}{\nd1}{type=number}
\answer{Nuevo numerador 2}{\nn2}{type=number}
\answer{Nuevo denominador 2}{\nd2}{type=number}

\answer{M�todo etapa 2}{\good2;\method2}{type=click}

\answer{Numerador de la suma}{\nsum}{type=number}
\answer{Denominador de la suma}{\dsum}{type=number}

\if{\step=2}{
 \text{nstep=r2,r3,r4,r5,r6,r7,r8,r9}
}

\text{test1=\test1<1 and \m1>0 and \m1=floor(\m1) and \m2>0 and \m2=floor(\m2)
    and \nn1=\num1*\m1 and \nd1=\den1*\m1
    and \nn2=\num2*\m2 and \nd2=\den2*\m2
    and \nd1=\nd2?1}
\if{\test1>0}{
 \text{new=\nn1,\nd1,\nn2,\nd2}
}

\condition{Las multiplicaciones son correctas}{\test1>0}

\if{\step=3 and \test1>0}{
 \text{nstep=r10}
}

\if{\step=4}{
 \text{nstep=r11,r12}
}

\condition{La suma est� bien}{\test1>0 and \nsum=\nn1+\nn2 and \dsum=\nd1}

\feedback{\step=1 and \reply1 notsametext ~\good1}{
 Las dos fracciones tienen denominadores diferentes. No podemos sumarlas
 directamente; es necesario reescribirlas para igualar su denominador.
}

\feedback{\step=2 and (\reply2 notsametext ~3 or \reply4 notsametext ~3)}{
 Para que una fracci�n siga siendo la misma, es necesario multiplicar
 su numerador y denominador por el mismo n�mero.
}

\feedback{\step=3 and (\m1=0 or \m2=0)}{
 �No puedes multiplicar el numerador y el denominador de una fracci�n
 por 0!
 <p>
 Vous obtiendriez une ``fraction'' \(0/(0)), qui n'a pas de sens.
}

\feedback{\step=3 and (\m1<0 or \m2<0)}{
 Lo siento pero en este ejercicio no est� permitido multiplicar por n�meros
 negativos.
}

\feedback{\step=3 and \m1>0 and \m2>0 and (\floor(\m1)!=\m1 or \floor(\m2)!=\m2)}{
 Debes multiplicar el numerador y el denominador de una fracci�n por un entero.
}

\feedback{\step=3 and \nd1=\den1*\m1 and \nd2=\den2*\m2 and \nd1!=\nd2}{
 El objetivo es igualar el denominador de las fracciones. Las
 multiplicaciones que has realizado no producen este resultado, ya que
 las fracciones producidas tienen denominadores diferentes. Multiplicaciones
 equivocadas.
}

\feedback{\step=3 and (\nd1!=\den1*\m1 or \nn1!=\num1*\m1 or
	\nd2!=\den2*\m2 or \nn2!=\num2*\m2)}{
  Error de c�lculo en las multiplicaciones.
}

\feedback{\step=3 and \reply10 notsametext ~\good2}{
 Para sumar dos fracciones con el mismo denominador, se suman los
 numeradores y se guarda el denominador com�n.
}

