target=correq3 correq4 correq5 corrinv3 corrinv4 corrinv5

#include "header.inc"
#define TIT Correspondencia
#if #TARGET (correq3)
 #define TIT2 igualdad
 #define CNT 3
#endif
#if #TARGET (correq4)
 #define TIT2 igualdad
 #define CNT 4
#endif
#if #TARGET (correq5)
 #define TIT2 igualdad
 #define CNT 5
#endif
#if #TARGET (corrinv3)
 #define TIT2 inversa
 #define CNT 3
 #define INVERSE 1
#endif
#if #TARGET (corrinv4)
 #define TIT2 inversa
 #define CNT 4
 #define INVERSE 1
#endif
#if #TARGET (corrinv5)
 #define TIT2 inversa
 #define CNT 5
 #define INVERSE 1
#endif

\title{TIT TIT2 CNT}
\integer{cnt=CNT}

\text{mulist=1,2,3,4,5,6,8,9,10,12}
#include "matrix.inc"

\text{sh=shuffle(items(\data))}
\text{choice=wims(translate internal / to , in \all[\sh[1..\cnt];1,2])}
\text{left=}
\text{right=}
\for{i=1 to \cnt}{
 \text{left=wims(append item \(\choice[\i;1]/(\choice[\i;2])) to \left)}
#ifdef INVERSE
 \text{right=wims(append item \(\choice[\i;4]/(\choice[\i;3])) to \right)}
#else
 \text{right=wims(append item \(\choice[\i;3]/(\choice[\i;4])) to \right)}
#endif
}
\text{size=50x50x50}

\statement{Establece la correspondencia entre
#ifdef INVERSE
 las fracciones y sus inversas.
#else
 las fracciones iguales.
#endif

<p><center>
\embed{reply 1,\size}
</center>
}

\reply{La correspondencia}{\left;\right}{type=correspond}

