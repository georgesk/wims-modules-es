\text{left=wims(line 2 to -1 of wims(record \style of src/styles))}
\text{left0=wims(nospace randitem(\left))}
\text{terms=wims(text select @ in \left0)}
\integer{tcnt=wims(charcnt \terms)}
\text{tlist=item(1..\ts,shuffle(\tcnt))}
\text{olist=item(1..\os,shuffle(\tcnt-1))}
\integer{redundant=\confparm4}

\text{vars=shuffle(a,b,c,d,r,s,t,x,y,z)}
\text{rt=sqrt(random(2,3,5))}
\text{list=wims(record \confparm1 of src/data)}
\text{nature=row(1,\list)}
\text{parms=row(2,\list)}
\text{obj=\parms[1]}
\text{sizex=\parms[2]}
\text{sizey=\parms[3]}
\text{sizex=\sizex =? 60}
\text{sizey=\sizey=? 35}
\text{size=\ts=0?30 30:\sizex \sizey}
\text{list=wims(rows2lines row(3..-1,\list))}
\integer{n=items(\list)}
\if{\n<=3}{
 \text{a=\list[1]}
 \text{b=\list[2]}
 \text{c=\list[3]}
 \text{randtype=computed}
 \if{\c<0}{
  \text{sh1=wims(values \c*x,-(\c)*x for x=\a to \b)}
 }{
  \text{sh1=wims(values \c*x for x=\a to \b)}
 }
 \text{sh=item(1..\tcnt+\redundant,shuffle(\sh1),shuffle(\sh1))}
}{
 \text{randtype=listed}
 \text{sh=item(1..\tcnt+\redundant,shuffle(\list),shuffle(\list))}
}
\text{sh=wims(mathsubst f=\vars[1] in \sh)}
\text{sh=wims(mathsubst g=\vars[2] in \sh)}
\text{sh=wims(mathsubst rt=\rt in \sh)}
\text{sh2=}
\for{s in \sh}{
 \text{s2=\s}
 \if{/ isin \s}{
  \text{s2=wims(replace internal / by /( in \s))}
 }
 \if{+ isin \s or - isin \s}{
  \text{s2=(\s)}
 }
 \text{sh2=wims(append item \s2 to \sh2)}
}
\text{sh=\sh2}
\text{empty=}

\text{Sh=wims(makelist \(x) for x in \sh)}

\text{ops=wims(declosing item(\confparm2,(-,+),(-,*,+),(-,*,/,+)))}
\text{op1=wims(replace internal * by &times; in \ops)}
\text{op1=wims(replace internal / by &divide; in \op1)}
\text{op1=wims(replace internal - by &minus; in \op1)}
\text{ops=wims(makelist \ops for i=1 to 10)}
\text{ops=shuffle(item(1..max(\confparm2+1,\tcnt),\ops))}
\text{Ops=wims(replace internal * by &times; in \ops)}
\text{Ops=wims(replace internal / by &divide; in \Ops)}
\text{Ops=wims(replace internal - by &minus; in \Ops)}

\integer{lcnt=wims(charcnt \left0)}
\text{left1=}
\text{left2=}
\text{left3=}
\integer{k=1}
\integer{n=1}
\text{qlist=}
\for{i=1 to \lcnt}{
 \text{c=wims(char \i of \left0)}
 \if{\c issametext @}{
  \text{left1=\left1 (\sh[\k]) }
  \if{\k isitemof \tlist}{
   \text{left2=\left2,}
   \text{left3=\left3,}
   \text{qlist=wims(append item \Sh[\k] to \qlist)}
  }{
   \text{left2=\left2 \Sh[\k] }
   \text{left3=\left3 \sh[\k] }
  }
  \integer{k=\k+1}
 }{
  \if{\c issametext #}{
   \text{left1=\left1 \ops[\n] }
   \if{\n isitemof \olist}{
    \text{left2=\left2,}
    \text{left3=\left3,}
    \text{qlist=wims(append item \Ops[\n] to \qlist)}
   }{
    \text{left2=\left2 \Ops[\n] }
    \text{left3=\left3 \ops[\n] }
   }
   \integer{n=\n+1}
  }
  {
   \text{left1=\left1\c}
   \text{left2=\left2\c}
   \text{left3=\left3\c}
  }
 }
}

\text{left2=wims(replace internal < by &#40; in \left2)}
\text{left2=wims(replace internal > by &#41; in \left2)}

\text{qlist=\ts>0 and \redundant>0?\qlist,\Sh[\tcnt+1..\tcnt+\redundant]}
\if{\os>0}{
 \integer{qtest1=items(\qlist)}
 \for{op in \op1}{
  \text{qlist=\op notin \qlist?\qlist,\op}
 }
 \integer{qtest2=items(\qlist)}
 \text{qlist=\qtest2=\qtest1?\qlist,randitem(\op1)}
}
\integer{qcnt=items(\left2)-1}

\text{left1=wims(translate internal <> to () in \left1)}
\text{sum=simplify(\left1)}
\if{/ isin \sum}{
 \text{sum=wims(replace internal / by /( in \sum))}
}

\text{rs=item(1..\qcnt,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16)}
\steps{\rs}

\statement{Rellene la siguiente ecuaci�n para convertirla en una igualdad.
<p><center>
\for{t=1 to \qcnt}{
 \left2[\t] \embed{r\t,\size 1}
}
\left2[\qcnt+1]

= \(\sum)
</center> <p>
Arrastre los t�rminos u operadores de abajo hasta los espacios en blanco de la ecuaci�n.
}

\answer{t1}{\t1;\qlist}{type=dragfill}
\answer{t2}{\t2}{type=dragfill}
\answer{t3}{\t3}{type=dragfill}
\answer{t4}{\t4}{type=dragfill}
\answer{t5}{\t5}{type=dragfill}
\answer{t6}{\t6}{type=dragfill}
\answer{t7}{\t7}{type=dragfill}
\answer{t8}{\t8}{type=dragfill}
\answer{t9}{\t9}{type=dragfill}
\answer{t10}{\t10}{type=dragfill}
\answer{t11}{\t11}{type=dragfill}
\answer{t12}{\t12}{type=dragfill}
\answer{t13}{\t13}{type=dragfill}
\answer{t14}{\t14}{type=dragfill}
\answer{t15}{\t15}{type=dragfill}
\answer{t16}{\t16}{type=dragfill}

\text{t=\t1,\t2,\t3,\t4,\t5,\t6,\t7,\t8,\t9,\t10,\t11,\t12,\t13,\t14,\t15,\t16}
\text{T=wims(replace internal \ by in \t)}
\text{T=wims(replace internal &minus; by - in \T)}
\text{T=wims(replace internal &times; by * in \T)}
\text{T=wims(replace internal &divide; by / in \T)}
\text{R=}
\text{r=}

\for{i=1 to \qcnt}{
  \text{R=\R \left3[\i] \T[\i]}
  \text{r=\r \left2[\i] \t[\i]}
}
\text{R=\R \left3[\qcnt+1]}
\text{r=\r \left2[\qcnt+1]}

\text{R=wims(translate internal <> to () in \R)}
\text{test=simplify(\R - (\sum))}
\text{test2=simplify(\R)}

\condition{Se verifica la ecuaci�n}{\test issametext 0}

\feedback{\test issametext \empty}{
 Respuesta incorrecta: \r no tiene sentido.
}
\feedback{\test notsametext \empty and \test notsametext 0}{
 C�lculo incorrecto: \r = \(\test2) en lugar de \(\sum).
}

