\title{Distancia y baricentro}
   utilizaci�n del producto escalar 
   para calcular la distancia desde un v�rtice de un tri�ngulo al    baricentro
\language{fr}
\author{Ren� Chaffard}
\email{chafrene@club-internet.fr}
\computeanswer{no}
\format{html}
\precision{1000}
\range{-5..5}

\text{sommet=random(a,b,c)}
   para hacer variar la presentaci�n del ejercicio

\text{liste=shuffle(A,B,C)}
\text{A=\liste[1]}
\text{B=\liste[2]}
\text{C=\liste[3]}

\integer{c1=randint(2..5)}
\integer{c2=randint(2..5)}
\integer{min=abs(\c1-\c2)+1}
\integer{max=\c1+\c2-1}
\integer{c3=randint(\min..\max)}
   los tres lados del tri�ngulo est� definidos por

\text{ps=simplify(((\c1)^2+(\c2)^2-(\c3)^2)/2)}
   el ps est� definido

\integer{p1=randint(1..4)*random(-1,1)}
\integer{p2=randint(1..4)*random(-1,1)}
\integer{p3=randint(1..4)*random(-1,1)}
\integer{sp=\p1+\p2+\p3}
\if {\sp=0} {\integer{p3=2*\p3}}
                        
   la somme des poids est non nulle
\integer{sp=\p1+\p2+\p3}

\text{k1=simplify(\p1/\sp)}
\text{k2=simplify(\p2/\sp)}
   les coef de la combinaison lin�aire � partir du sommet

\text{d=simplify(sqrt((\k1*\c1)^2+(\k2*\c2)^2+2*\k1*\k2*\ps))}
   calcul de la distance
 \text{hsg=\A G} 
   hsg est utilis�e pour l'affichage
\text{longueur=shuffle(\(\A\B = \c1 ) , \(\B\C = \c3) ,  \(\C\A = \c2))}
\steps{
reply 1,reply 2
reply 3
reply 4
}

 �tape 1 : coeficients de la combinaison lin�aire
 �tape 2 : calcul du produit scalaire
 �tape 3 : calcul de la distance

\statement{
Se considera el tri�ngulo \(ABC) tal que :<br/>
<p style="text-align:center">\longueur[1],  \longueur[2], \longueur[3]. 
</p>
Sea \(G) el baricentro de (\(\A) , \p3), (\(\B) , \p1), (\(\C) , \p2).
Se pide calcular la distancia \(\hsg).
<br/>
<hr/>
\if {\step=1}
 {Determinar en primer lugar \(k_1) et \(k_2) tales que :
  <p style="text-align:center">\( \overrightarrow{\A G} = k_{1} 
  \overrightarrow{\A B} + k_{2} \overrightarrow{\A C} )</p>
 }
\if {\step=2} 
 {Calcular el producto escalar :
<p style="text-align:center">\( \overrightarrow{\A\B} \cdot \overrightarrow{\A\C} )</p>
 }
\if {\step=3}
 {Sabiendo que \(k_1 = \k1) et \(k_2 = \k2)  y que el producto escalar 
 \( \overrightarrow{\A\B} \cdot \overrightarrow{\A\C} ) es igual a \ps, 
 no hay m�s que una respuesta.
<p>
<i>En caso de necesidad, se podr� utilizar la funci�n  sqrt (ra�z cuadrada). Ej : sqrt(2) = \( \sqrt{2} )</i>
</p>
}
}

\answer{\(k_1) }{\k1}
\answer{\(k_2) }{\k2}
\answer{Producto escalar}{\ps}
\answer{Distancia \(\hsg)}{\d}{type=formal}

\help{
Si \(G) es el baricentro de \((A , a)), \((B , b)) et \((C , c)) entonces, para todo punto \(O) :
<p style="text-align:center">\( \overrightarrow{OG}=\frac{1}{a+b+c}(a\overrightarrow{OA}+b\overrightarrow{OB}+c\overrightarrow{OC}) )</p>

Ser� necesario elegir unn punto \(O) en funci�n de la cuesti�n propuesta.
}