target=corr3 corr4 corr5 corr6 corr7

#include "header.inc"
#include "confparm.inc"
#define TIT1 Correspondance

#if #TARGET (corr3)
 \title{Correspondencia 3}
 \integer{cnt=3}
#endif
#if #TARGET (corr4)
 \title{Correspondencia 4}
 \integer{cnt=4}
#endif
#if #TARGET (corr5)
 \title{Correspondencia 5}
 \integer{cnt=5}
#endif
#if #TARGET (corr6)
 \title{Correspondencia 6}
 \integer{cnt=6}
#endif
#if #TARGET (corr7)
 \title{Correspondencia 7}
 \integer{cnt=7}
#endif

#include "data.inc"
\text{f1=\given}
\text{f2=\ask}

\text{namelist=n�meros, s�mbolos, nombres, masas}
\text{sxs=60,60,150,100}

\text{sh=item(1..\cnt,shuffle(\datacnt))}
\text{data=row(\sh,\data)}
\text{left=column(\f1,\data)}
\text{right=column(\f2,\data)}

\statement{Establece la correspondencia correcta entre los siguientes \namelist[\f1]
y \namelist[\f2] de �tomos.
<p><center>
 \embed{r1,30x\sxs[\f1]x\sxs[\f2]}
</center>
}

\answer{La correspondencia}{\left;\right}{type=correspond}
