target=fill3 fill4 fill5 fill6 fill8 fill10 fill14 fill20 fill33 fill54 fill62 fill103 fill108

#include "header.inc"
#include "confparm.inc"
#define TIT1 Fill

\integer{redund=0}
  
#if #TARGET (fill3)
# define CNT 3
#endif
#if #TARGET (fill4)
# define CNT 4
#endif
#if #TARGET (fill5)
# define CNT 5
#endif
#if #TARGET (fill6)
# define CNT 6
#endif
#if #TARGET (fill8)
# define CNT 8
#endif
#if #TARGET (fill10)
# define CNT 10
#endif
#if #TARGET (fill14)
# define CNT 14
#endif
#if #TARGET (fill20)
# define CNT 20
#endif
#if #TARGET (fill33)
# define CNT 3 + 3
 \integer{redund=3}
#endif
#if #TARGET (fill54)
# define CNT 5 + 4
 \integer{redund=4}
#endif
#if #TARGET (fill62)
# define CNT 6 + 2
 \integer{redund=2}
#endif
#if #TARGET (fill103)
# define CNT 10 + 3
 \integer{redund=3}
#endif
#if #TARGET (fill108)
# define CNT 10 + 8
 \integer{redund=8}
#endif
\title{Coloca CNT casillas}
\integer{cnt=wims(word 1 of CNT)}

#include "position.inc"

\integer{cnt=min(\cnt,\pdatacnt)}
\text{sh=item(1..\cnt+\redund,shuffle(\pdatacnt))}
\text{sh=wims(sort numeric items \sh)}
\text{data=row(\sh,\pdata)}
\text{symb=\data[;2]}
\text{rsymb=\redund>0 and \pdatacnt>\cnt?;\symb[\cnt+1..\cnt+\redund]:}
text{symb=wims(replace internal , by </small>,<small> in <small>\symb</small>)}
\text{fields=}
\integer{xs=35}
\integer{ys=30}
\for{t=1 to \cnt}{
 \integer{x=\data[\t;3]>8?\data[\t;4]*\xs+\xs*2+1:\data[\t;4]*\xs+1}
 \integer{y=\data[\t;3]>8?\data[\t;3]*\ys+6:\data[\t;3]*\ys+1}
 \text{fields=\fields
r\t,\x x \y}
}

\text{size=\xs-1 x \ys-1}
\text{answers=item(1..\cnt,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20)}
\steps{\answers}

\statement{Coloca los s�mbolos dados en la tabla siguiente.
<p> <table border=0><tr><td>
\special{imagefill \imagedir/table2.gif,700x360,\size
\fields
}
</td></tr></table>
}
\text{size=\xs x \ys-1}

\answer{Box 1}{\symb[1]\rsymb}{type=dragfill}
\answer{Box 2}{\symb[2]}{type=dragfill}
\answer{Box 3}{\symb[3]}{type=dragfill}
\answer{Box 4}{\symb[4]}{type=dragfill}
\answer{Box 5}{\symb[5]}{type=dragfill}
\answer{Box 6}{\symb[6]}{type=dragfill}
\answer{Box 7}{\symb[7]}{type=dragfill}
\answer{Box 8}{\symb[8]}{type=dragfill}
\answer{Box 9}{\symb[9]}{type=dragfill}
\answer{Box 10}{\symb[10]}{type=dragfill}
\answer{Box 11}{\symb[11]}{type=dragfill}
\answer{Box 12}{\symb[12]}{type=dragfill}
\answer{Box 13}{\symb[13]}{type=dragfill}
\answer{Box 14}{\symb[14]}{type=dragfill}
\answer{Box 15}{\symb[15]}{type=dragfill}
\answer{Box 16}{\symb[16]}{type=dragfill}
\answer{Box 17}{\symb[17]}{type=dragfill}
\answer{Box 18}{\symb[18]}{type=dragfill}
\answer{Box 19}{\symb[19]}{type=dragfill}
\answer{Box 20}{\symb[20]}{type=dragfill}
