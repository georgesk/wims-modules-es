target=order3 order4 order5 order6

#include "header.inc"
#include "confparm.inc"
#define TIT1 Order

#if #TARGET (order3)
# define CNT 3
#endif
#if #TARGET (order4)
# define CNT 4
#endif
#if #TARGET (order5)
# define CNT 5
#endif
#if #TARGET (order6)
# define CNT 6
#endif
\title{Ordenar CNT}
\integer{cnt=CNT}

#include "data.inc"

\text{given=\given notitemof 2,3?randitem(2,3)}

\text{namelist=n�meros, s�mbolos, nombres, masas}
\text{sxs=60,60,120,100}

\text{sh=item(1..\cnt,shuffle(\datacnt))}
\text{sh=wims(sort numeric items \sh)}
\text{data=row(\sh,\data)}
\text{data=column(\given,\data)}

\statement{Coloca los siguientes �tomos en orden creciente, arrastr�ndolos
a las casillas vac�as.
<p> <table border=0><tr><td>
\for{i=1 to \cnt}{
 \embed{r\i,\sxs[\given]x30}
}
</td></tr></table>
}

\answer{Elemento 1}{\data[1]}{type=dragfill}
\answer{Elemento 2}{\data[2]}{type=dragfill}
\answer{Elemento 3}{\data[3]}{type=dragfill}
#if CNT>3
 \answer{Elemento 4}{\data[4]}{type=dragfill}
#endif
#if CNT>4
 \answer{Elemento 5}{\data[5]}{type=dragfill}
#endif
#if CNT>5
 \answer{Elemento 6}{\data[6]}{type=dragfill}
#endif
