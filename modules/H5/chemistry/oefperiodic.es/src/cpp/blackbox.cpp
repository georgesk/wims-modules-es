target=blackbox blackbox2

#include "header.inc"
#include "confparm.inc"

#if #TARGET (blackbox)
\title{Casilla vac�a}
\text{image=table1.gif}
#endif
#if #TARGET (blackbox2)
\title{Casilla vac�a II}
\text{image=table2.gif}
#endif

#include "data.inc"
#include "position.inc"

\integer{pick=randint(1..\datacnt)}
\text{dataline=\data[\pick;]}
\integer{pick=item(1,\dataline)}
\text{pdataline=\Pdata[\pick;]}
\integer{xs=30}
\integer{ys=30}
\integer{x=\pdataline[4]*\xs}
\integer{y=\pdataline[3]*\ys}
\integer{y=\pdataline[3]>8?\y+5}
\integer{x=\pdataline[3]>8?\x+\xs*2}
\text{blist=}
\for{i=1 to \end}{
 \text{blist=\blist r,\x,\y,\x+\xs,\y+\ys;}
 \if{\i=\pick}{\text{glist=r,\x,\y,\x+\xs,\y+\ys}}
}
\text{good=\dataline[\ask]}
\text{good=\ask=3?\good;\Data[;3]}
\text{namelist=n�mero at�mico, s�mbolo, nombre, masa at�mica}
\text{a=r1,r2,r3,r4}
\steps{\a[\ask]}

\statement{En la tabla peri�dica de los elementos mostrada m�s abajo,
una casilla est� vac�a. �Cu�l es el/la \namelist[\ask] del elemento
correspondiente a esa casilla?
<p><center>
\draw{600,360}{
copy 0,0,-1,-1,-1,-1,\imagedir/\image
frect \x,\y,\x+\xs,\y+\ys,black
}
</center>
}

\answer{El n�mero at�mico es}{\good}{type=number}
\answer{El s�mbolo es}{\good}{type=case}
\answer{El nombre del elemento es}{\good}{type=nocase}
\answer{La masa at�mica es}{\good}{type=number}
