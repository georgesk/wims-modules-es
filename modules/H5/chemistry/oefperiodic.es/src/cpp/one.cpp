target=one prec next

#include "header.inc"
#include "confparm.inc"

#if #TARGET (one)
 \title{Un elemento}
 \integer{start=1}
 \text{cnt=0,0}
 \text{connector=}
#endif
#if #TARGET (prec)
 \title{Elemento previo}
 \integer{start=2}
 \text{cnt=0,-1}
 \text{connector=inmediatamente anterior a aquel}
 \text{candouble=yes}
 \text{selective=no}
#endif
#if #TARGET (next)
 \title{Elemento siguiente}
 \integer{start=2}
 \text{cnt=-1,0}
 \text{connector=inmediatamente posterior a aquel}
 \text{candouble=yes}
 \text{selective=no}
#endif

#include "data.inc"

\text{f1=\given}
\text{f2=\ask}

\text{namelist=n�mero, s�mbolo, nombre, masa}

\text{pick=randint(\start..\datacnt)}
\text{data1=row(\cnt[1]+\pick,\data)}
\text{data2=row(\cnt[2]+\pick,\data)}
\text{left=column(\f1,\data1)}
\text{right=column(\f2,\data2)}

\steps{reply \f2}

\statement{�Cu�l es el \namelist[\f2] del elemento \connector cuyo
\namelist[\f1] es \left?
}

\answer{El n�mero at�mico es}{\right}{type=number}
\answer{El s�mbolo es}{\right}{type=case}
\answer{El nombre del elemento es}{\right;\totname}{type=nocase}
\answer{La masa at�mica}{\right}{type=number}
