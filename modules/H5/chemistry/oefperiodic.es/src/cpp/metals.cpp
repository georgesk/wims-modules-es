target=metals4 metals6 metals8 metals10

#include "header.inc"
#include "confparm.inc"
#define TIT Metals and non-metals

#if #TARGET (metals4)
# define TOT 4
#endif
#if #TARGET (metals6)
# define TOT 6
#endif
#if #TARGET (metals8)
# define TOT 8
#endif
#if #TARGET (metals10)
# define TOT 10
#endif
\title{Metal y no metal TOT}

#include "position.inc"

\integer{tot=TOT}
\integer{beg=item(\tot,1,1,1,1,2,2,2,2,3,3,3,3,4,4)}
\integer{cut=random(\beg..\tot-\beg)}
\integer{length=\tot-\beg+1}
\text{given=\given notitemof 2,3?randitem(2,3)}
\integer{xsize=item(\given-1,50,100)}
\text{metals=column(1,wims(select \pdata where column 5 isitemof 2,3,7))}
\text{nonmetals=column(1,wims(select \pdata where column 5 notitemof 2,3,7))}
\text{metals=\data[\metals;\given]}
\text{nonmetals=\data[\nonmetals;\given]}
\text{metals=item(1..\cut,shuffle(\metals))}
\text{nonmetals=item(1..\tot-\cut,shuffle(\nonmetals))}
\text{size=\xsize[]x30x\length}

\statement{Clasifica los elementos mostrados m�s abajo en "metales" y
"no metales".

<p>
Metales: \embed{reply 1,\size}
<p>
No metales: \embed{reply 2,\size}
}

\answer{Metals}{\rmet;\metals}{type=dragfill}
\answer{Non-metals}{\rnon;\nonmetals}{type=dragfill}

\text{totans=\rmet,\rnon}
\text{noans=wims(listcomplement \totans in \metals,\nonmetals)}
\text{totans=\totans,\noans}

\text{clean=}
\text{check=}
\for{t=1 to \tot}{
 \text{ans=\totans[\t]}
 \if{\ans isitemof \rmet}{
  \text{clean=\clean a metal,}
  \text{check=\ans isitemof \metals?\check 1,:\check 0,}
 }{
  \if{\ans isitemof \rnon}{
   \text{clean=\clean a non-metal,}
   \text{check=\ans isitemof \nonmetals?\check 1,:\check 0,}
  }{
   \text{clean=\clean neighther metal nor non-metal,}
   \text{check=\check 0,}
  }
 }
}

\condition{\totans[1] is \clean[1]}{\check[1]=1}
\condition{\totans[2] is \clean[2]}{\check[2]=1}
\condition{\totans[3] is \clean[3]}{\check[3]=1}
\condition{\totans[4] is \clean[4]}{\check[4]=1}
#if TOT > 4
\condition{\totans[5] is \clean[5]}{\check[5]=1}
\condition{\totans[6] is \clean[6]}{\check[6]=1}
#endif
#if TOT > 6
\condition{\totans[7] is \clean[7]}{\check[7]=1}
\condition{\totans[8] is \clean[8]}{\check[8]=1}
#endif
#if TOT > 8
\condition{\totans[9] is \clean[9]}{\check[9]=1}
\condition{\totans[10] is \clean[10]}{\check[10]=1}
#endif
