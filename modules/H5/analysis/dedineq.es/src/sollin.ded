\title{Linear solve I}
\author{Gang Xiao}
\email{xiao@unice.fr}
\language{en}
\options{nofloat askresult}
\methods{ineq/addterm, ineq/mulnum, ineq/divnum, ineq/exsides}
\minsteps{2}

\integer{a=random(2..20)*random(-1,1)}
\integer{b=random(2..20)*random(-1,1)}
\integer{c=random(2..20)*random(-1,1)}
\integer{d=random(1..20)*random(-1,1)}

\function{left=\a*x + \d}
\text{sign=random(<,<=,>,>=)}
\function{right=\b*x + \c*y}
\text{rightvar=}
\equivalence{\left \sign \right}

\statement{
Solve for \y the inequality \(\left \sign \right).
<p>
The goal of the exercise is to transform the given inequality into an
equivalent one where the left part is \y, and the right part is an expression
of \x.
}

\text{cut=wims(translate internal <=> to , , in \equivalence)}
\text{newleft=item(1,\cut)}
\text{newright=item(2,\cut)}
\text{rightvar=wims(varlist \newright)}
\text{empty=}

\condition{\newleft issametext y and
 (\rightvar issametext x or \rightvar issametext \empty)}

