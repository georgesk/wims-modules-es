\title	{Calcular elementos de una matriz 3x3}
\language{en}
\author	{XIAO Gang}
\email	{xiao@unice.fr}
\format	{html}
\range  {-5..5}
\precision{10000}

\integer{range=10}
\integer{half=\range/2}
\matrix{A=
randint(-\range..\range),randint(-\range..\range),randint(-\range..\range)
randint(-\range..\range),randint(-\range..\range),randint(-\range..\range)
randint(-\range..\range),randint(-\range..\range),randint(-\range..\range)
}
\matrix{B=2*randint(-\half..\half)+1,randint(-\range..\range),randint(-\range..\range)
2*randint(-\half..\half),2*randint(-\half..\half)+1,randint(-\range..\range)
2*randint(-\half..\half),2*randint(-\half..\half),2*randint(-\half..\half)+1
}
\text{shuf1=shuffle(3)}
\text{shuf2=shuffle(3)}
\matrix{B=pari(print(vecextract([\B],[\shuf1],[\shuf2])))}
\integer{type=random(1,2)}
\if{\type=1}{
 \matrix{C=pari(print([\B]*[\A]))}
 \text{B1=row(1,\B)}
 \text{B2=row(2,\B)}
 \text{B3=row(3,\B)}
 \text{C1=row(1,\C)}
 \text{C2=row(2,\C)}
 \text{C3=row(3,\C)}
}{
 \matrix{C=pari(print([\A]*[\B]))}
 \matrix{B1=wims(items2lines column(1,\B))}
 \matrix{B2=wims(items2lines column(2,\B))}
 \matrix{B3=wims(items2lines column(3,\B))}
 \matrix{C1=wims(items2lines column(1,\C))}
 \matrix{C2=wims(items2lines column(2,\C))}
 \matrix{C3=wims(items2lines column(3,\C))}
}

\statement{Tenemos una matriz \(A), 3&times;3 tal que
<p><center>
\if{\type=1}{
 \([\B1]*A = [\C1]) , <br>
 \([\B2]*A = [\C2]) , <br>
 \([\B3]*A = [\C3]) .
}{
 \(A*[\B1] = [\C1]) , 
 \(A*[\B2] = [\C2]) , 
 \(A*[\B3] = [\C3]) .
}
</center> <p>
Calcular \(A).
}

\answer{A}{\A}{type=matrix}
