\title{Valor absoluto}
\language{en}
\range{-5..5}
\author{Gang XIAO}
\email{xiao@unice.fr}
\computeanswer{no}
\format{html}
\precision{10000}

\text{addcomm=No. La suma no es conmutativa}
\text{nozero=No. El elemento nulo 0 no existe}
\text{addasso=No. La suma no es asociativa}
\text{adddef=No. La suma no est� bien definida}
\text{addneg=No. Hay elementos que no tienen opuesto}
\text{multdef=No. El producto por escalar no est� bien definido}
\text{multasso=No. El producto por escalar no es asociativo}
\text{multone=No. Al multiplicar por el elemento unidad 1 cambia el vector}
\text{distrib=No. No verifica la propiedad distributiva}
\text{yes=S�. Es un espacio vectorial}
\text{choices=\yes,\multdef,\multasso,\distrib}
\text{sub1=<sub>1</sub>}
\text{sub2=<sub>2</sub>}

\statement{Sea <i>S</i> el conjunto de parejas (x,y) de n�mero reales.
Definimos la suma y producto por escalares en <i>S</i>
como sigue:<ul>
<li>Para cualquier (x\sub1,y\sub1) y (x\sub2,y\sub2) perteneciente a <i>S</i>, 
 definimos (x\sub1,y\sub1)+(x\sub2,y\sub2) = (x\sub1+x\sub2,y\sub1+y\sub2).
<li>Para cualquier (x,y) perteneciente a <i>S</i> y cualquier n�mero real <i>a</i>,
 definimos
 <i>a</i>(x,y) = (|<i>a</i>|x,|<i>a</i>|y).
</ul>
�Es <i>S</i> con la estructura definida m�s arriba un espacio vectorial sobre
<b>R</b>?}

\choice{La respuesta}{\distrib}{\choices}
\solution{En efecto, tenemos (1+(-1))(x,y)=(0,0), pero 1(x,y)+(-1)(x,y)=(2x,2y).}
