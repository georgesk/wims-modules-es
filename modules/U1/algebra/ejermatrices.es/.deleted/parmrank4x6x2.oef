\title	{Parametric rank 4x6x2}
\author	{XIAO Gang}
\email	{xiao@unice.fr}
\format	{html}

\integer{rank=random(2,3)}
\integer{rows=4}
\integer{cols=6}
\integer{range=5}
\text{p1=u}
\text{p2=v}
\integer{ps=2}

\integer{r1=\rank+1}
\integer{c1=\rank+1}
\integer{r2=min(\rows,\rank+2)}
\integer{c2=\rank+2}

\integer{uval=random(-20..20)}
\integer{vval=random(-20..20)}
\text{rowsh=shuffle(\rows)}
\text{colsh=shuffle(\cols)}
\text{rand=((RANDOM(\range)+1)*(RANDOM(2)*2-1))}
\text{trans=random(0,1)=1?m=mattranspose(m);}
\text{m=pari(a1=matrix(\rows,\rank,x,y,\rand);
 a2=matrix(\rank,\cols,x,y,\rand);
 if(\rank>1,
  for(i=1,\rank-1,for(j=1,\rank-i,a1[i,j]=0;a2[i,j]=0))
 );
 m=a1*a2; m[\r1,\c1]+=(RANDOM(2)*2-1)*(\p1-(\uval));
 m[\r2,\c2]+=(RANDOM(2)*2-1)*(\p2-(\vval));
 m=vecextract(m,[\rowsh],[\colsh]);
 \trans print(m)
)}

\integer{mm=random(1,2)}
\text{minmax=item(\mm,minimum,maximum)}
\text{eqneq=equal to, different from}
\text{and=and}
\text{or=or}
\text{andor=\and,\or}
\text{eq1=item(\mm,\eqneq)}
\text{eq2=item(\mm,\eqneq)}
\integer{rankmin=\rank}
\integer{rankmax=\r1=\r2?\rank+1:\rank+2}
\text{connect=\and}
\text{connect=\mm=2 and \rankmax=\rank+1?\or}

\statement{Consider the following parametrized matrix.
<p><center>
 \( A = [\m] \)
</center> <p>
<b>Fill-in</b>:
Following the values of the parameters \(\p1) and \(\p2),
the rank of \A is at least \embed{reply 1,3} and at most
\embed{reply 2,3}.
<p>
The \minmax rank is reached when \(\p1) is \embed{choice 1}
\embed{reply 3,4} \embed{choice 2} \(\p2) is \embed{choice 3}
\embed{reply 4,4}.
}

\answer{Minimum rank}{\rankmin}{type=numeric}
\answer{Maximum rank}{\rankmax}{type=numeric}
\choice{\p1 is}{\eq1}{\eqneq}
\answer{Value of \p1}{\uval}{type=numeric}
\choice{\andor}{\connect}{\andor}
\choice{\p2 is}{\eq2}{\eqneq}
\answer{Value of \p2}{\vval}{type=numeric}

