\title	{Partial multiplication 5x5}
\author	{XIAO Gang}
\email	{xiao@unice.fr}
\format	{html}
\computeanswer{yes}

\integer{s=5}
\text{random=random}
\matrix{A=pari(print(matrix(\s,\s,x,y,if(x>y,tz,(\random(9)+1)*(2*\random(2)-1)))))}
\matrix{B=pari(print(matrix(\s,\s,x,y,if(x<y,tz,(\random(9)+1)*(2*\random(2)-1)))))}
\text{sh1=shuffle(\s)}
\text{sh2=shuffle(\s)}
\text{sh3=shuffle(\s)}
\text{sh4=shuffle(\s)}
\matrix{A=pari(print(vecextract([\A],[\sh1],[\sh2])))}
\matrix{B=pari(print(vecextract([\B],[\sh3],[\sh4])))}
\text{AT=wims(texmath [\A])}
\text{BT=wims(texmath [\B])}
\text{AT=wims(replace internal tz by ? in \AT)}
\text{BT=wims(replace internal tz by ? in \BT)}
\text{CT=wims(texmath [c11,c12,c13,c14,c15;c21,c22,c23,c24,c25;c31,c32,c33,c34,c35;c41,c42,c43,c44,c45;c51,c52,c53,c54,c55])}
\text{i=wims(positionof item 1 in \sh1)}
\text{j=wims(positionof item 1 in \sh4)}
\text{good=c\i\j}
\integer{c=pari(([\A]*[\B])[\i,\j])}
\steps{
reply 1
reply 2}

\statement{We have an equation of multiplication of matrices \s&times;\s as
follows, where the question marks represent unknown coefficients.
<p><center>
 \(\AT\BT = \CT)
</center> <p>
\if{\step=1}{
 Step 1. There is only one determinable coefficient in the product matrix.
 It is \embed{reply 1,4}. <br>
 (Type <tt>c11</tt> for \(c11) for example.)
}{
 Step 2. The determinable coefficient is \(c\i\j) = \embed{reply 2,6}.
}}

\answer{The detreminable coefficient}{\good}{type=atext}
\answer{c\i\j}{\c}{type=number}
