\title	{Square root 2x2*}
\language{en}
\author	{XIAO Gang}
\email	{xiao@unice.fr}
\format	{tex}
\range  {-5..5}
\precision{10000}

\text{X=A}
\text{I=I}
\integer{aa=random(1..9)*random(-1,1)}
\integer{bb=random(1..9)*random(-1,1)}
\integer{cc=random(1..9)*random(-1,1)}
\integer{dd=random(1..9)*random(-1,1)}
\integer{a=\aa*(\aa)+(\bb)*(\cc)}
\integer{b=(\aa+(\dd))*(\bb)}
\integer{c=(\aa+(\dd))*(\cc)}
\integer{d=(\dd)^2+(\bb)*(\cc)}

\statement{Find a matrix $A=\pmatrix{a&b\cr c&d}$
such that
$$ A^2 = \pmatrix{\a&\b\cr\c&\d}, $$
where the entries
$a, b, c, d$ must be non-zero integers.
}
\answer	{<i>a</i>}  {\A}
\answer	{<i>b</i>}  {\B}
\answer	{<i>c</i>}  {\C}
\answer	{<i>d</i>}  {\D}

\condition{A^2 is the given matrix}
{(\A)^2+(\B)*(\C)=\a and \B*(\A+(\D))=\b and
 \C*(\A+(\D))=\c and (\D)^2+(\B)*(\C)=\d}

\condition{a,b,c,d are non-zero integers}
{\A*(\B)*(\C)*(\D)!=0 and floor(\A)=\A and floor(\B)=\B and
floor(\C)=\C and floor(\D)=\D}

