target= reduitsom2,reduitsom3,reduitsom4

\language{fr}
\author{Guerimand Fabrice}
\email{fwguerima@free.fr}
\computeanswer{no}
\format{html}
\precision{10000}
\text{size=50x50x50}

#if #TARGET (reduitsom2)
\integer{nombre=2}
\title{Ecriture r�duite d'une somme 2.}
#endif

#if #TARGET (reduitsom3)
\integer{nombre=3}
\title{Ecriture r�duite d'une somme 3.}
#endif

#if #TARGET (reduitsom4)
\integer{nombre=4}
\title{Ecriture r�duite d'une somme 4.}
#endif

\integer{c=random(2,3,5,6,7,10)}
\text{b=randint(2..15),randint(2..15),randint(2..15),randint(2..15)}
\text{a=randint(-1,1)*randint(2..15),randint(-1,1)*randint(2..15),randint(-1,1)*randint(2..15),randint(-1,1)*randint(2..15)}

\text{enonce=}
\integer{res=0}
\for{i=1 to \nombre}{
\integer{tmp=(\b[\i])^2*\c}
\integer{tmp2=\a[\i]}
\text{sg=\tmp2>0 and \i>1?+:}
\text{enonce=\enonce \sg \tmp2\sqrt{\tmp}}
\integer{res=\res+(\tmp2)*(\b[\i])}
}

\text{rep=\res*sqrt(\c)}

\statement{Ecrire le nombre \(\enonce) sous la forme \(a\sqrt{b}) o� \(b) est un entier le plus petit possible.<br>
(vous devez taper sqrt(2) pour \(\sqrt{2}) ) }

\hint{Faire apparaitre \(\sqrt{\c}) dans chacun des termes de la somme.}

\reply{R�ponse}{\rep}{type=algexp}
