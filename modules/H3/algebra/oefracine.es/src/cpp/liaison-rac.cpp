target= liaison3,liaison4,liaison5

\language{fr}
\author{Guerimand Fabrice}
\email{fwguerima@free.fr}
\computeanswer{no}
\format{html}
\precision{10000}
\text{size=50x50x50}

#if #TARGET (liaison3)
\integer{nombre=3}
\title{Racines et nombres 3.}
#endif

#if #TARGET (liaison4)
\integer{nombre=4}
\title{Racines et nombres 4.}
#endif

#if #TARGET (liaison5)
\integer{nombre=5}
\title{Racines et nombres 5.}
#endif

\integer{N1=random(4..20)}
\text{R1=\(\sqrt{\N1})}

\integer{R2=random(1..9)}
\real{R2=\R2/10}
\real{N2=\R2^2}

\integer{R3=random(2..13)}
\integer{N3=\R3^2}

\integer{R4=random(2..15)}
\text{N4=\(\R4^2)}

\integer{R5=random(2..13)}
\integer{N5=\R5^2}
\text{R5=\(\frac{1}{\R5})}
\text{N5=\(\frac{1}{\N5})}

\integer{a=random(2,4,8,10,11)}
\integer{b=random(3,9,7,13)}
\integer{A=\a^2}
\integer{B=\b^2}
\text{R6=\(\frac{\a}{\b})}
\text{N6=\(\frac{\A}{\B})}

\integer{R7=random(1,-1)*random(2..9)}
\integer{N7=2*\R7}
\text{R7=\(10^{\R7})}
\text{N7=\(10^{\N7})}

\integer{a=random(2..9)}
\integer{R8=random(1,-1)*random(2..9)}
\integer{N8=2*\R8}
\text{R8=\(\a^{\R8})}
\text{N8=\(\a^{\N8})}


\text{don=\N1;\R1,\N2;\R2,\N3;\R3,\N4;\R4,\N5;\R5,\N6;\R6,\N7;\R7,\N8;\R8}
\text{don=shuffle(\don)}
\text{don=wims(replace internal ; by , in \don)}
\text{nb=}
\text{ra=}
\for{i=1 to \nombre}{
\text{tmp=item(\i*2-1,\don)}
\text{nb=\nb,\tmp}
\text{tmp=item(\i*2,\don)}
\text{ra=\ra,\tmp}
}
\text{nb=wims(nonempty item \nb)}
\text{ra=wims(nonempty item \ra)}

\statement{Mettez en relation les nombres de la premi�re colonne avec leurs racines carr�es dans la deuxi�me.
<p><center>
\embed{reply 1,\size}
</center>
}

\reply{La correspondance}{\nb;\ra}{type=correspond}
