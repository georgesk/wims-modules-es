\title{Rectangle et racine carr�e}
\language{fr}
\range{-5..5}
\author{Buffet Philippe & Guerimand Fabrice}
\email{fwguerima@free.fr}
\computeanswer{no}
\format{html}
\precision{10000}

\matrix{list=A,B,C,D
E,F,G,H
I,J,K,L}
\text{list=randrow(\list)}
\text{list=shuffle(\list)}

\text{list=A,B,C,D}
\text{A=item(1,\list)}
\text{B=item(2,\list)}
\text{C=item(3,\list)}
\text{D=item(4,\list)}

\integer{a=random(2..30)}
\integer{b=floor(sqrt(\a))}
\integer{b=random(1..\b)}

\text{peri=simplify(4*sqrt(\a))}
\integer{aire=\a-\b^2}
\text{diag=simplify(sqrt(2*\a+2*\b^2))}
\integer{ch=random(1..2)}
\if{\ch=1}{
\text{affangle=\B\A\C}
\integer{angle=arctan((sqrt(\a)-\b)/(sqrt(\a)+\b))*180/pi}
}{
\text{affangle=\B\C\A}
\integer{angle=arctan((sqrt(\a)+\b)/(sqrt(\a)-\b))*180/pi}
}

\statement{\(\A\B\C\D) est un rectangle tel que \(\A\B=\sqrt{\a}+\b) et \(\B\C=\sqrt{\a}-\b).
<ul>
<li>Calculer la valeur exacte de son p�rim�tre.<br>
R�ponse : \embed{r1,15}</li>
<li>Calculer la valeur exacte de son aire.<br>
R�ponse : \embed{r2,15}</li>
<li>Calculer la longueur exacte de la diagonale \(\A\C).<br>
R�ponse : \embed{r3,15}</li>
<li>Calculer la mesure de l'angle \(\widehat{\affangle}) � un degr� pr�s.<br>
R�ponse : \embed{r4,15}</li>
</ul>
<b>Attention</b> :
<ul>
<li>Les r�sultats doivent �tre le plus simplifi�s possibles.</li>
<li>Pour �crire \(\sqrt{2}) tapez sqrt(2).</li>
</ul>}

\reply{P�rim�tre}{\peri}{type=litexp}
\reply{Aire}{\aire}{type=litexp}
\reply{Diagonale \(\A\C)}{\diag}{type=litexp}
\reply{\(\widehat{\affangle})}{\angle}{type=litexp}
