\title{Vitesse et compteur}
\language{fr}
\range{-5..5}
\author{Bernadette PERRIN-RIOU}
\email{bpr@math.u-psud.fr}
\computeanswer{no}
\format{html}
\precision{100}

\real{v=randint(0..24)*5}
\integer{hd=randint(0..10)}
\integer{ha=randint(\hd+3..\hd+7)}
\integer{t=\ha-\hd}
\integer{d=\v*\t}
\integer{kmd=randint(10000..10500)}
\integer{kma=\kmd+\d}

\steps{reply1
reply2
reply3}

\statement{Un automobiliste part de son domicile � \hd h00, 
son compteur indique \kmd km.
 Il revient � \ha h00, son compteur 
marque \kma km. <p>
\if {\step=1}{Quelle distance a-t-il parcourue ? }
\if {\step=2}{Pendant combien de temps a-t-il roul� ?}
\if {\step=3}{En effet, il a parcouru \d km et a roul� pendant \t h.  Quelle a �t� sa vitesse moyenne horaire ?}

<p><b><font color="purple"> Consigne : les unit�s sont demand�es.</font></b>}

\answer{La distance parcourue est de}{\d km} {type=units}
\answer{Il a roul� pendant}{\t h} {type=units}
\answer{Sa vitesse moyenne horaire est de}{\v km/h}{type=units}

\hint{\if{\step=1}{La distance parcourue est la diff�rence entre ce qu'indique son compteur � l'arriv�e et ce qu'il indique  au d�part.}
\if{\step=2}{Le temps de parcours est la diff�rence entre l'heure 
d'arriv�e et l'heure de d�part. }
\if{\step=3}{La vitesse moyenne horaire est �gale � la distance parcourue 
divis�e par le temps pendant lequel il a roul�}}
