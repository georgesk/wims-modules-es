\title{fonction lin�aire 2}
\language{fr}
\range{-5..5}
\author{Guerimand Fabrice}
\email{fwguerima@free.fr}
\computeanswer{no}
\format{html}
\precision{10000}

\text{nom=randitem(f,g,h)}
\integer{nb1=random(1,-1)*random(1..20)}
\integer{nb2=random(1,-1)*random(1..40)}
\integer{tmp=random(2..20)}
\text{nb2=simplify(\nb2/\tmp)}
\integer{nb3=random(1,-1)*random(1..20)}
\integer{tmp=random(2..20)}
\integer{nb4=random(1,-1)*random(1..20)}
\text{nb4=simplify(\nb4/\tmp)}
\text{tmp=simplify(\nb3*\nb2/\nb1)}
\text{nb4=randitem(\tmp,\nb4)}
\text{bad=Oui,Non}
\text{good=\nb1*\nb4-\nb2*\nb3=0?1:2}
\text{good=item(\good,\bad)}

\text{rap1=simplify(\nb2/\nb1)}
\text{rap1=(\rap1 issametext \nb2/\nb1)?\frac{\nb2}{\nb1}:\frac{\nb2}{\nb1}=\rap1}
\text{rap2=simplify(\nb4/\nb3)}
\text{rap2=(\rap2 issametext \nb4/\nb3)?\frac{\nb4}{\nb3}:\frac{\nb4}{\nb3}=\rap2}

\statement{L'image de \(\nb1) par la fonction lin�aire \(\nom) est \(\nb2) et l'image de \(\nb3) est \(\nb4).<p>
\(\nom) peut-elle �tre une fonction lin�aire?}

\choice{Votre r�ponse}{\good}{\bad}

\solution{On peut r�soudre l'exercice de deux fa�ons:
<ul>
<li><b>M�thode 1 :</b> faire un tableau de proportionnalit�:<p>
<table border="1">
  <tr> 
    <td> 
      <div align="center"><b>x</b></div>
    </td>
    <td>\(\nb1)</td>
    <td>\(\nb3)</td>
  </tr>
  <tr> 
    <td> 
      <div align="center"><b>\nom (x)</b></div>
    </td>
    <td>\(\nb2)</td>
    <td>\(\nb4)</td>
  </tr>
</table>
Se demander si la fonction peut �tre lin�aire revient � se demander si le tableau est un tableau de proportionnalit�. On calcule donc les rapports des nombres de la seconde ligne sur ceux de la premi�re. On trouve :
<center> \(\rap1) et \(\rap2).</center> Ces deux fractions 
\if{\good=Oui}{sont �gales, donc le tableau est proportionnel et la fonction peut �tre lin�aire.}{ ne sont pas �gales, donc le tableau n'est pas proportionnel et la fonction n'est pas lin�aire.}</li>
<li><b>M�thode 1 :</b> on utilise la notation fonctionnelle :<p> 
Lorsque la fonction est lin�aire, le rapport entre les nombres et leurs images est constant. On calcule les rapports :<p>
\(\frac{\nom(\nb1)}{\nb1}=\rap1) et \(\frac{\nom(\nb3)}{\nb3}=\rap2)<p>
Ces deux fractions 
\if{\good=Oui}{sont �gales, donc le tableau est proportionnel et la fonction peut �tre lin�aire.}{ ne sont pas �gales, donc le tableau n'est pas proportionnel et la fonction n'est pas lin�aire.}</li>
</ul> }
