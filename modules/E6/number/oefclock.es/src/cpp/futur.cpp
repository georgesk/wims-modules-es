target=futur1_4 futur1_5 futur1_6 futur2_4 futur2_5 futur2_6

#include "header.inc"
#if #TARGET (futur1_4)
  \title{Futuro I-4}
  \integer{give=4}
  #define FUTUR1 1
#endif

#if #TARGET (futur1_5)
  \title{Futuro I-5}
  \integer{give=5}
  #define FUTUR1 1
#endif

#if #TARGET (futur1_6)
  \title{Futuro I-6}
  \integer{give=6}
  #define FUTUR1 1
#endif

#if #TARGET (futur2_4)
  \title{Futuro II-4}
  \integer{give=4}
  #define FUTUR2 1
#endif

#if #TARGET (futur2_5)
  \title{Futuro II-5}
  \integer{give=5}
  #define FUTUR2 1
#endif

#if #TARGET (futur2_6)
  \title{Futuro II-6}
  \integer{give=6}
  #define FUTUR2 1
#endif

\text{h=h}
\integer{more=random(6..57)}
\integer{size=120}
\integer{nowh=random(12..23)}
\integer{nowm=random(1..59)}
\integer{now=\nowh*60+\nowm}
\text{nowclock=slib(draw/clock \nowh:\nowm,\size,1,exactmin nosecond)}
\integer{fut=\now+\more}
\text{list=-\more,60,\more*2,-\more*2,
-\more-10,-\more-60,-\more-30,-\more-15,-\more-5,
-\more+5,-\more+10,-\more+15,-\more+20,-\more+30,
\more+2, \more-2, \more+3, \more-3,
\more+60,\more-60,\more+59,\more+61,\more-59,\more-61,
\more+10,\more-10,\more+5,\more-5,\more+30,\more-30,
\more+15,\more-15}
\integer{n=items(\list)}
#ifdef FUTUR1
 \text{list=shuffle(wims(values x+\now for x in \list))}
 \integer{fut=\now+\more}
#else
 \text{list=shuffle(wims(values \now-x for x in \list))}
 \integer{fut=\now-\more}
#endif
\text{list=wims(listuniq \fut,item(1..\give,\list))}
\text{sh=shuffle(\give)}
\text{gp=wims(positionof item 1 in \sh)}
\text{bad=}
\text{mins=}
\for{i=1 to \give}{
 \integer{f=item(item(\i,\sh),\list)}
 \integer{fh=floor(\f/60)}
 \integer{fm=\f%60}
 \text{clock=slib(draw/clock \fh:\fm,\size,1,exactmin nosecond)}
 \text{dr=draw(\size,\size
\clock)}
 \text{bad=wims(append item <img align=middle src=\dr> to \bad)}
 \if{\i=\gp}{\text{good=<img align=middle src=\dr>}}
}

\statement{Arrastre el reloj correcto a la casilla vac�a: <p>
<table border=0>
<tr><td nowrap>
Son las</td><td>
#ifdef FUTUR1
 \draw{\size,\size}{\nowclock}.
 </td><td nowrap>
 Dentro de \more minutos, ser�n las</td><td>
 \embed{reply 1,\size \size}
#else
 \embed{reply 1,\size \size}
 </td><td nowrap>.
 Dentro de \more minutos, ser�n las</td><td>
 \draw{\size,\size}{\nowclock}
#endif
</td><td>.</td></tr>
</table></center>
}

\answer{El reloj correcto}{\good;\bad}{type=dragfill}

