target=posdec1 reldec1 posdec2 reldec2

#include "header.inc"
\text{randint=randint}

#if #TARGET (posdec1) || #TARGET (reldec1)
 #if #TARGET (posdec1)
  \title{Decimales positivos I}
  \text{size=40x30}
 #else
  \title{Decimales relativos I}
  \text{size=45x30}
  #define ADDNEG 1
 #endif
 \text{data=wims(values x/10 for x=1 to 99)}
#endif

#if #TARGET (posdec2) || #TARGET (reldec2)
 #if #TARGET (posdec2)
  \title{Decimales positivos II}
  \text{size=60x30}
  \integer{lim=4}
 #else
  \title{Decimales relativos II}
  \text{size=65x30}
  \integer{lim=8}
  #define ADDNEG 1
 #endif
 \text{data1=wims(values \randint(19)+x/10 for x=1 to 9)}
 \text{data2=wims(values \randint(199)/10+x/100 for x=1 to 9)}
 \text{data=\data1[1..\lim],\data2[1..\lim]}
#endif

\text{name=n�meros}
\text{Name=N�meros}

#include "common.inc"