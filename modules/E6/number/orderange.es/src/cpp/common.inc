\if{\confparm1>=3 and \confparm1<=8}
{\integer{\cnt=\confparm1}}{\integer{\cnt=4}}
\integer{orient=random(1,2)}
\if{\confparm2=2}{\integer{orient=1}}
\if{\confparm2=3}{\integer{orient=2}}
\text{sign=item(\orient,<,>)}
\text{sorder=item(\orient,,reverse)}
\text{smallbig=menor,mayor}
\text{dir=creciente, decreciente}

\text{data=wims(nonempty items \data)}

#ifdef ADDNEG
 \integer{half=(\cnt+3)/2}
 \text{data1=shuffle(\data)}
 \text{data2=shuffle(wims(replace , by ,- in -\data))}
 \text{data=\data1[1..\half],\data2[1..\half]}
#endif

\text{data=shuffle(\data)}
\text{data=\orient=1?wims(sort numeric items \data[1..\cnt]):
	wims(sort reverse numeric items \data[1..\cnt])
}
 \text{good=}
\if{\name iswordof fraction}{
 \text{data=wims(replace internal / by \over in \data)}
 \for{i=1 to \cnt}{
  \text{good=wims(append item \({\data[\i]}) to \good)}
 \text{s=es}
 }
}{
 \for{i=1 to \cnt}{\text{good=wims(append item \(\data[\i]) to \good)}}
}
\text{rlist=item(1..\cnt,r1,r2,r3,r4,r5,r6,r7,r8)}
\steps{\rlist}

\statement{Ordenar los/las \name de abajo en orden \dir[\orient], de  \smallbig[\orient]
a \smallbig[3-\orient].
<p><center>
<table border=0><tr>
<td>\embed{r1,\size}<td>
\for{t=2 to \cnt}{
 <td> \sign <td>\embed{\rlist[\t],\size}
}
</table></center>
}

\answer{\Name 1}{\good[1]}{type=dragfill}
\answer{\Name 2}{\good[2]}{type=dragfill}
\answer{\Name 3}{\good[3]}{type=dragfill}
\answer{\Name 4}{\good[4]}{type=dragfill}
\answer{\Name 5}{\good[5]}{type=dragfill}
\answer{\Name 6}{\good[6]}{type=dragfill}
\answer{\Name 7}{\good[7]}{type=dragfill}
\answer{\Name 8}{\good[8]}{type=dragfill}