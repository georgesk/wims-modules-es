target=posfrac1 posfrac2 posfrac3 posfrac4 posfrac5 relfrac1 relfrac2 relfrac3 relfrac4 relfrac5

#include "header.inc"

#if #TARGET (posfrac1) || #TARGET (relfrac1)
 #if #TARGET (posfrac1)
  \title{Fracciones positivas I}
  \text{size=35x55}
 #else
  \title{Fracciones relativas I}
  \text{size=40x55}
  #define ADDNEG 1
 #endif
 \text{data=randomrow(
1/3,2/3,4/3,5/3,7/3,8/3,10/3,11/3,13/3,14/3,16/3,17/3,19/3,20/3
1/5,2/5,3/5,4/5,6/5,7/5,8/5,9/5,11/5,12/5,13/5,14/5,16/5,17/5,18/5,19/5
1/7,2/7,3/7,4/7,5/7,6/7,8/7,9/7,10/7,11/7,12/7,13/7,15/7,16/7,17/7,18/7,19/7
)}
#endif

#if #TARGET (posfrac2) || #TARGET (relfrac2)
 #if #TARGET (posfrac2)
  \title{Fracciones positivas II}
  \text{size=40x55}
 #else
  \title{Fracciones relativas II}
  \text{size=45x55}
  #define ADDNEG 1
 #endif
 \integer{num=random(7,11,13,17)}
 \text{data=wims(values x,x+\num,x+2*\num for x=1 to \num-1)}
 \text{data=wims(replace , by ,\num/ in \num/\data[2..-1])}
 \text{data=pari([\data])}
#endif

#if #TARGET (posfrac3) || #TARGET (relfrac3)
 #if #TARGET (posfrac3)
  \title{Fracciones positivas III}
  \text{size=35x55}
 #else
  \title{Fracciones relativas III}
  \text{size=40x55}
  #define ADDNEG 1
 #endif
 \text{data=
1/2,3/2,5/2,
1/3,2/3,4/3,5/3,
1/4,3/4,5/4,
1/5,2/5,3/5,4/5
}
#endif

#if #TARGET (posfrac4) || #TARGET (relfrac4)
 #if #TARGET (posfrac4)
  \title{Fracciones positivas IV}
  \text{size=40x55}
 #else
  \title{Fracciones relativas IV}
  \text{size=45x55}
  #define ADDNEG 1
 #endif
 \text{data=
1/2,3/2,5/2,7/2,9/2,11/2,
1/3,2/3,4/3,5/3,7/3,8/3,10/3,11/3,
1/4,3/4,5/4,7/4,9/4,11/4,
1/5,2/5,3/5,4/5,6/5,7/5,8/5,9/5,11/5,12/5,
1/6,5/6,7/6,11/6,
1/7,2/7,3/7,4/7,5/7,6/7,8/7,9/7,10/7,11/7,12/7,
1/8,3/8,5/8,7/8,9/8,11/8,
1/9,2/9,4/9,5/9,7/9,8/9,10/9,11/9,
1/10,3/10,7/10,9/10,11/10,
1/11,2/11,3/11,4/11,5/11,6/11,7/11,8/11,9/11,10/11,12/11,
1/12,5/12,7/12,11/12
}
#endif

#if #TARGET (posfrac5) || #TARGET (relfrac5)
 #if #TARGET (posfrac5)
  \title{Fracciones positivas V}
  \text{size=40x55}
 #else
  \title{Fracciones relativas V}
  \text{size=45x55}
  #define ADDNEG 1
 #endif
 \integer{denom=random(20,24,30,36,40,48,50,54,60)}
 \text{data=wims(values x,x+\denom,x+2*\denom for x=1 to \denom-1)}
 \text{data=pari([\data]/\denom)}
#endif

\text{name=fracciones}
\text{Name=Fracciones}

#include "common.inc"