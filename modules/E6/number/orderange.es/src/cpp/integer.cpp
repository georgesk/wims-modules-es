target=posint1 relint1 posint2 relint2 posint3 relint3

#include "header.inc"
\text{randint=randint}

#if #TARGET (posint1) || #TARGET (relint1)
 #if #TARGET (posint1)
  \title{N�meros positivos (peque�os)}
  \text{size=35x30}
 #else
  \title{N�meros relativos (peque�os)}
  \text{size=40x30}
  #define ADDNEG 1
 #endif
 \text{data=wims(values x for x=1 to 20)}
#endif

#if #TARGET (posint2) || #TARGET (relint2)
 #if #TARGET (posint2)
  \title{N�meros positivos (medianos)}
  \text{size=45x30}
  \integer{lim=4}
 #else
  \title{N�meros relativos (medianos)}
  \text{size=50x30}
  \integer{lim=9}
  #define ADDNEG 1
 #endif
 \text{data1=shuffle(wims(values \randint(9)*10+10+x for x=0 to 9))}
 \text{data2=shuffle(wims(values \randint(90)*10+100+x for x=0 to 9))}
 \text{data=\data1[1..\lim],\data2[1..\lim]}
#endif

#if #TARGET (posint3) || #TARGET (relint3)
 #if #TARGET (posint3)
  \title{N�meros positivos (grandes)}
  \text{size=75x30}
  \integer{lim=3}
 #else
  \title{N�meros relativos (grandes)}
  \text{size=80x30}
  \integer{lim=7}
  #define ADDNEG 1
 #endif
 \text{data1=shuffle(wims(values 10*\randint(900)+1000+x for x=0 to 9))}
 \text{data2=shuffle(wims(values 10*\randint(9000)+10000+x for x=0 to 9))}
 \text{data3=shuffle(wims(values 10*\randint(90000)+100000+x for x=0 to 9))}
 \text{data=\data1[1..\lim],\data2[1..\lim],\data3[1..\lim]}
#endif

\text{name=n�meros}
\text{Name=N�meros}

#include "common.inc"